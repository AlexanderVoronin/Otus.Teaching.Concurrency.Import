﻿namespace Otus.Teaching.Concurrency.Import.Loader.Settings
{
    public class LoaderSettings
    {
        public string FilePath { get; set; }

        public int ThreadsQuantity { get; set; }

        public int GeneratorDataQuantity { get; set; }

        public string GeneratorDataFileProcessPath { get; set; }

        public string GeneratorDataFileMode { get; set; }

        public bool Runbenchmark { get; set; }
    }
}
