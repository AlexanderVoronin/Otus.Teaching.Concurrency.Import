﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Loader.DataParsers
{
    public class XmlCustomerParser : IDataParser<IList<Customer>, IEnumerable<XElement>>
    {
        public IList<Customer> Parse(IEnumerable<XElement> source)
        {
            List<Customer> customers = new List<Customer>();

            XmlSerializer serializer = new XmlSerializer(typeof(Customer));
            foreach (XElement element in source)
            {
                Customer customer = (Customer)serializer.Deserialize(element.CreateReader());
                customers.Add(customer);
            }

            return customers;
        }
    }
}
