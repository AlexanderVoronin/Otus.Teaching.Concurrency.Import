﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Loader.DataParsers
{
    public class CsvCustomerParser : IDataParser<IList<Customer>, string>
    {
        public IList<Customer> Parse(string source)
        {
            List<Customer> customers = CsvSerializer.DeserializeFromString<List<Customer>>(source); 

            return customers;
        }
    }
}
