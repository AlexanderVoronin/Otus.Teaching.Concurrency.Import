﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Interfaces;
using Otus.Teaching.Concurrency.Import.Loader.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Linq;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class XmlDataLoaderWithThreadPool : IDataLoader
    {
        private readonly LoaderSettings _settings;
        private readonly IDataParser<IList<Customer>, IEnumerable<XElement>> _customerXmlDataParser;
        private readonly IServiceProvider _serviceProvider;
        private readonly IRetryService _retryService;

        public XmlDataLoaderWithThreadPool(IOptions<LoaderSettings> settings,
            IDataParser<IList<Customer>, IEnumerable<XElement>> customerXmlDataParser,
            IServiceProvider serviceProvider,
            IRetryService retryService)
        {
            _settings = settings?.Value;
            _customerXmlDataParser = customerXmlDataParser;
            _serviceProvider = serviceProvider;
            _retryService = retryService;
        }

        public void LoadData() => this.LoadData(_settings.FilePath);

        public void LoadData(string filePath)
        {
            var xmlDoc = XDocument.Load(filePath);
            var users = xmlDoc.Descendants("Customer");

            // Divide the users into batches
            var batchSize = (int)Math.Ceiling((double)users.Count() / _settings.ThreadsQuantity);
            IEnumerable<IEnumerable<XElement>> usersBatches = users.Batch(batchSize);

            WaitHandle[] manualEvents = new WaitHandle[_settings.ThreadsQuantity];
            int count = 0;
            foreach (var usersBatch in usersBatches)
            {
                var handle = new EventWaitHandle(false, EventResetMode.ManualReset);
                manualEvents[count] = handle;
                ThreadPool.QueueUserWorkItem(new WaitCallback((object state) =>
                {
                    _retryService.Do(() => this.LoadPartition(usersBatch), TimeSpan.FromSeconds(3));
                    handle.Set();
                }));
                count++;
            }
            WaitHandle.WaitAll(manualEvents);
        }

        private void LoadPartition(IEnumerable<XElement> data)
        {
            IList<Customer> customers = _customerXmlDataParser.Parse(data);
            ICustomerRepository customerRepository = _serviceProvider.GetRequiredService<ICustomerRepository>();
            customerRepository.AddCustomers(customers);
            customerRepository.Save();
        }
    }
}
