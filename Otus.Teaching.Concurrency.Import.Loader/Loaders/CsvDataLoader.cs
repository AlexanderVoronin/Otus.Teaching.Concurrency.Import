﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Interfaces;
using Otus.Teaching.Concurrency.Import.Loader.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class CsvDataLoader : IDataLoader
    {
        private readonly LoaderSettings _settings;
        private readonly IDataParser<IList<Customer>, string> _customerCsvDataParser;
        private readonly IServiceProvider _serviceProvider;
        private readonly IRetryService _retryService;

        public CsvDataLoader(IOptions<LoaderSettings> settings,
            IDataParser<IList<Customer>, string> customerCsvDataParser,
            IServiceProvider serviceProvider,
            IRetryService retryService)
        {
            _settings = settings?.Value;
            _customerCsvDataParser = customerCsvDataParser;
            _serviceProvider = serviceProvider;
            _retryService = retryService;
        }

        public void LoadData() => this.LoadData(_settings.FilePath);

        public void LoadData(string filePath)
        {
            string[] csvLines = File.ReadAllLines(filePath);
            string columnNames = csvLines[0];
            string[] dataLines = csvLines.Skip(1).ToArray();

            // Divide the users into batches
            var batchSize = (int)Math.Ceiling((double)csvLines.Length / _settings.ThreadsQuantity);
            IEnumerable<IEnumerable<string>> usersBatches = dataLines.Batch(batchSize);

            Parallel.ForEach(usersBatches,
                (usersBatch) => _retryService.Do(() => this.LoadPartition(usersBatch, columnNames), TimeSpan.FromSeconds(1)));
        }

        private void LoadPartition(IEnumerable<string> data, string columnNames)
        {
            IList<Customer> customers = _customerCsvDataParser.Parse($"{columnNames}{Environment.NewLine}{string.Join(Environment.NewLine, data)}");
            ICustomerRepository customerRepository = _serviceProvider.GetRequiredService<ICustomerRepository>();
            customerRepository.AddCustomers(customers);
            customerRepository.Save();
        }
    }
}
