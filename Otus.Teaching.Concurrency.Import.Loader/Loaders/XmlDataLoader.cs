﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Interfaces;
using Otus.Teaching.Concurrency.Import.Loader.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Linq;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class XmlDataLoader : IDataLoader
    {
        private readonly LoaderSettings _settings;
        private readonly IDataParser<IList<Customer>, IEnumerable<XElement>> _customerXmlDataParser;
        private readonly IServiceProvider _serviceProvider;
        private readonly IRetryService _retryService;

        public XmlDataLoader(IOptions<LoaderSettings> settings, 
            IDataParser<IList<Customer>, IEnumerable<XElement>> customerXmlDataParser,
            IServiceProvider serviceProvider,
            IRetryService retryService)
        {
            _settings = settings?.Value;
            _customerXmlDataParser = customerXmlDataParser;
            _serviceProvider = serviceProvider;
            _retryService = retryService;
        }

        public void LoadData() => this.LoadData(_settings.FilePath);

        public void LoadData(string filePath)
        {
            var xmlDoc = XDocument.Load(filePath);
            var users = xmlDoc.Descendants("Customer");

            // Divide the users into batches
            var batchSize = (int)Math.Ceiling((double)users.Count() / _settings.ThreadsQuantity);
            IEnumerable<IEnumerable<XElement>> usersBatches = users.Batch(batchSize);

            Barrier barrier = new Barrier(_settings.ThreadsQuantity + 1);
            foreach (var usersBatch in usersBatches)
            {
                Thread thread = new Thread(() =>
                {
                    _retryService.Do(() => this.LoadPartition(usersBatch), TimeSpan.FromSeconds(1));
                    barrier.SignalAndWait();
                });
                thread.Start();
            }
            barrier.SignalAndWait();
        }

        private void LoadPartition(IEnumerable<XElement> data)
        {
            IList<Customer> customers = _customerXmlDataParser.Parse(data);
            ICustomerRepository customerRepository = _serviceProvider.GetRequiredService<ICustomerRepository>();
            customerRepository.AddCustomers(customers);
            customerRepository.Save();
        }
    }
}
