﻿using System;

namespace Otus.Teaching.Concurrency.Import.Loader.Interfaces
{
    public interface IRetryService
    {
        void Do(Action action, TimeSpan retryInterval, int maxAttemptCount = 3);
    }
}