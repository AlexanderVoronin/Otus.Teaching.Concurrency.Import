﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Npgsql;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.DataParsers;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Settings;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml.Linq;
using Otus.Teaching.Concurrency.Import.Loader.Interfaces;
using Otus.Teaching.Concurrency.Import.Loader.Helpers;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    [MemoryDiagnoser]
    [Orderer(BenchmarkDotNet.Order.SummaryOrderPolicy.SlowestToFastest)]
    [RankColumn]
    [SimpleJob(RunStrategy.Monitoring, warmupCount: 1, launchCount: 1, iterationCount: 2)]
    public class Benchmark
    {
        private IServiceProvider _serviceProvider;
        private XmlDataLoader _xmlDataLoader;
        private XmlDataLoaderWithThreadPool _xmlDataLoaderWithThreadPool;
        private CsvDataLoader _csvDataLoader;
        private XmlGenerator _xmlGenerator;
        private CsvGenerator _csvGenerator;
        private LoaderSettings _loaderSettings;
        private string _xmlFilePath;
        private string _csvFilePath;


        [GlobalSetup]
        public void RegisterServices()
        {
            IConfiguration config = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .Build();

            var serviceCollection = new ServiceCollection();
            serviceCollection.AddTransient<XmlDataLoaderWithThreadPool>()
                                    .AddTransient<XmlDataLoader>()
                                    .AddTransient<CsvDataLoader>()
                                    .AddTransient<XmlGenerator>()
                                    .AddTransient<CsvGenerator>()
                                    .AddSingleton<IRetryService, RetryService>();

            serviceCollection.AddDbContext<AppDbContext>(contextLifetime: ServiceLifetime.Transient, optionsLifetime: ServiceLifetime.Transient)
                                    .AddTransient<IDbConnection>((sp) => new NpgsqlConnection(config.GetConnectionString("PostgreCustomer")));

            serviceCollection.AddTransient<IDataParser<IList<Customer>, IEnumerable<XElement>>, XmlCustomerParser>()
                    .AddTransient<IDataParser<IList<Customer>, string>, CsvCustomerParser>()
                    .AddTransient<ICustomerRepository, CustomerRepository>()
                    .Configure<LoaderSettings>(options => config.GetSection("LoaderSettings").Bind(options));

            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Use the DI container to resolve the dependency
            _serviceProvider = serviceProvider;
            _xmlDataLoader = serviceProvider.GetRequiredService<XmlDataLoader>();
            _xmlDataLoaderWithThreadPool = serviceProvider.GetRequiredService<XmlDataLoaderWithThreadPool>();
            _csvDataLoader = serviceProvider.GetRequiredService<CsvDataLoader>(); ;
            _xmlGenerator = serviceProvider.GetRequiredService<XmlGenerator>(); ;
            _csvGenerator = serviceProvider.GetRequiredService<CsvGenerator>(); ;
            _loaderSettings = serviceProvider.GetRequiredService<IOptions<LoaderSettings>>().Value;
            _xmlFilePath = _loaderSettings.FilePath.Replace(".csv", ".xml");
            _csvFilePath = _loaderSettings.FilePath.Replace(".xml", ".csv");

            _xmlGenerator.Generate(_xmlFilePath, _loaderSettings.GeneratorDataQuantity);
            _csvGenerator.Generate(_csvFilePath, _loaderSettings.GeneratorDataQuantity);
        }

        [IterationCleanup]
        public void CleanCustomerTable()
        {
            using (var dbConnection = _serviceProvider.GetRequiredService<IDbConnection>())
            {
                dbConnection.Open();
                using (IDbCommand command = dbConnection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM customers";
                    command.ExecuteNonQuery();
                }
            }
        }

        [Benchmark]
        public void LoadXmlDataWithThread()
        {
            _xmlDataLoader.LoadData(_xmlFilePath);
        }

        [Benchmark]
        public void LoadXmlDataWithThreadPool()
        {
            _xmlDataLoaderWithThreadPool.LoadData(_xmlFilePath);
        }

        [Benchmark]
        public void LoadCsvDataWithThreadPool()
        {
            _csvDataLoader.LoadData(_csvFilePath);
        }
    }
}
