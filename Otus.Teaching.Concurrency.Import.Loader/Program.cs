﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using BenchmarkDotNet.Running;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Npgsql;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Initializer;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.DataGenerators;
using Otus.Teaching.Concurrency.Import.Loader.DataParsers;
using Otus.Teaching.Concurrency.Import.Loader.Helpers;
using Otus.Teaching.Concurrency.Import.Loader.Interfaces;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Settings;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    internal class Program
    {       
        private static void Main(string[] args)
        {
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var host = AppStartup();

            IDBInitializer dBInitializer = host.Services.GetRequiredService<IDBInitializer>();
            dBInitializer.Initialize();

            LoaderSettings appSettings = host.Services.GetRequiredService<IOptions<LoaderSettings>>().Value;

            if (appSettings.Runbenchmark)
                BenchmarkRunner.Run<Benchmark>();
            else
            {
                IDataFileGenerator dataFileGenerator = host.Services.GetRequiredService<IDataFileGenerator>();
                dataFileGenerator.GenerateCustomersDataFile(appSettings.FilePath, appSettings.GeneratorDataQuantity);
                IDataLoader dataLoader = host.Services.GetRequiredService<IDataLoader>();
                dataLoader.LoadData();
            }

            Console.WriteLine("Data was load.");
            Console.ReadKey();
        }

        private static IConfiguration BuildConfig()
        {
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            return config;
        }

        private static IHost AppStartup()
        {
            var config = BuildConfig();
            
            var host = Host.CreateDefaultBuilder()
                        .ConfigureServices((context, services) =>
                        {
                            services.AddDbContext<AppDbContext>(contextLifetime: ServiceLifetime.Transient, optionsLifetime: ServiceLifetime.Transient)
                                    .AddTransient<IDbConnection>((sp) => new NpgsqlConnection(config.GetConnectionString("PostgreCustomer")))
                                    .AddTransient<IDBInitializer, DBInitializer>();

                            services.AddTransient<IDataParser<IList<Customer>, IEnumerable<XElement>>, XmlCustomerParser>()
                                    .AddTransient<IDataParser<IList<Customer>, string>, CsvCustomerParser>()
                                    .AddTransient<ICustomerRepository, CustomerRepository>()
                                    .AddSingleton<IRetryService, RetryService>()
                                    .Configure<LoaderSettings>(options => config.GetSection("LoaderSettings").Bind(options));

                            string generatorDataFileMode = config.GetValue<string>("LoaderSettings:GeneratorDataFileMode");
                            switch (generatorDataFileMode)
                            {
                                case "Internal":
                                    services.AddTransient<IDataFileGenerator, InternalDataFileGenerator>();
                                    break;
                                case "External":
                                    services.AddTransient<IDataFileGenerator, ExternalDataFileGenerator>();
                                    break;
                                default:
                                    throw new Exception($"Unrecognized GeneratorDataFileMode: {generatorDataFileMode}");
                            }

                            string filePath = config.GetValue<string>("LoaderSettings:FilePath");
                            DirectoryInfo directoryInfo = new DirectoryInfo(filePath);
                            switch (directoryInfo.Extension)
                            {
                                case ".xml":
                                    services.AddTransient<IDataGenerator, XmlGenerator>()
                                            .AddTransient<IDataLoader, XmlDataLoader>();
                                    break;
                                case ".csv":
                                    services.AddTransient<IDataGenerator, CsvGenerator>()
                                            .AddTransient<IDataLoader, CsvDataLoader>();
                                    break;
                                default:
                                    throw new Exception($"Unrecognized FilePath extension: {directoryInfo.Extension}");
                            }
                        })
                        .Build();

            return host;
        }
    }
}