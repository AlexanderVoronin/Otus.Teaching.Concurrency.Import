﻿using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName;
        private static int _dataCount = 100;

        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;

            Console.WriteLine("Generating data...");

            string fileExtension = new DirectoryInfo(_dataFileName).Extension;
            var generator = GeneratorFactory.GetGenerator(fileExtension);

            generator.Generate(_dataFileName, _dataCount);

            Console.WriteLine($"Generated data in {_dataFileName}...");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                DirectoryInfo path = new DirectoryInfo(args[0]);
                if (string.IsNullOrEmpty(path.Extension))
                    _dataFileName = Path.Combine(_dataFileDirectory, $"{args[0]}.xml");
                else
                    _dataFileName = Path.Combine(_dataFileDirectory, $"{args[0]}");
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }

            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out _dataCount))
                {
                    Console.WriteLine($"Data must be integer {args[1]}");
                    return false;
                }
            }

            return true;
        }
    }
}