using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".xml":
                    return new XmlDataGenerator();
                case ".csv":
                    return new CsvGenerator();
                default:
                    throw new System.Exception($"File extension \"{fileExtension}\" doesn't support for generating data.");
            }  
        }
    }
}