﻿namespace Otus.Teaching.Concurrency.Import.DataAccess.Initializer
{
    public interface IDBInitializer
    {
        void Initialize();
    }
}
