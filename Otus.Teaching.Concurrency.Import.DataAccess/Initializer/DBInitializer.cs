﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Initializer
{
    public class DBInitializer : IDBInitializer
    {
        private readonly AppDbContext _appDBContext;

        public DBInitializer(AppDbContext appDbContext)
        {
            _appDBContext = appDbContext;
        }

        public void Initialize()
        {
            if (_appDBContext.Database.GetPendingMigrations().Count() != 0)
            {
                _appDBContext.Database.Migrate();
            }
        }
    }
}
