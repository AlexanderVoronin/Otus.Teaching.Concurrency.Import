using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly DbSet<Customer> _dbSet;
        private readonly AppDbContext _db;

        public CustomerRepository(AppDbContext db)
        {
            _db = db;
            _dbSet = db.Set<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            _dbSet.Add(customer);
        }

        public void AddCustomers(IEnumerable<Customer> customers)
        {
            _dbSet.AddRange(customers);
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}