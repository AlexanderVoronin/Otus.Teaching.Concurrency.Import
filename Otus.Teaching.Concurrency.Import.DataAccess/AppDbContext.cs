﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.IO;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
        {
            this.ThreadID = Thread.CurrentThread.ManagedThreadId;
        }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            this.ThreadID = Thread.CurrentThread.ManagedThreadId;
        }

        public int ThreadID { get; set; }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
                var connectionString = configuration.GetConnectionString("PostgreCustomer");
                optionsBuilder.UseNpgsql(new NpgsqlConnection(connectionString))
                            .UseSnakeCaseNamingConvention();
            }
        }
    }
}
