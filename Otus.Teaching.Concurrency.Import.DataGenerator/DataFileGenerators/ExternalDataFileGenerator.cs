﻿using Otus.Teaching.Concurrency.Import.Loader.Interfaces;
using System;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Loader.DataGenerators
{
    public class ExternalDataFileGenerator : IDataFileGenerator
    {
        public void GenerateCustomersDataFile(string filePath, int dataQuantity)
        {
            string executablePath = filePath;
            string arguments = $"\"{filePath}\" {dataQuantity}";

            Process process = new Process();
            process.StartInfo.FileName = executablePath;
            process.StartInfo.Arguments = arguments;

            process.Start();
            process.WaitForExit();

            int exitCode = process.ExitCode;

            if (exitCode != 0)
                throw new Exception($"Exception while execute process by path \"{filePath}\". Exit code = {exitCode}.");
        }
    }
}
