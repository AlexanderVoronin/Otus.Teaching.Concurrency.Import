﻿using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Loader.Interfaces;

namespace Otus.Teaching.Concurrency.Import.Loader.DataGenerators
{
    public class InternalDataFileGenerator : IDataFileGenerator
    {
        private readonly IDataGenerator _dataGenerator;
        
        public InternalDataFileGenerator(IDataGenerator dataGenerator)
        {
            _dataGenerator = dataGenerator;
        }

        public void GenerateCustomersDataFile(string filePath, int dataQuantity) 
            => _dataGenerator.Generate(filePath, dataQuantity);
    }
}
