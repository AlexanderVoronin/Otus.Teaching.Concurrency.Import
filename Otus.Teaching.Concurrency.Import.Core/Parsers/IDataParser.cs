﻿namespace Otus.Teaching.Concurrency.Import.Core.Parsers
{
    public interface IDataParser<TOut, TIn>
    {
        TOut Parse(TIn source);
    }
}