﻿namespace Otus.Teaching.Concurrency.Import.Loader.Interfaces
{
    public interface IDataFileGenerator
    {
        void GenerateCustomersDataFile(string filePath, int dataQuantity);
    }
}
