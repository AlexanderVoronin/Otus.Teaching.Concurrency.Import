﻿namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDataLoader
    {
        void LoadData();
        void LoadData(string filePath);
    }
}